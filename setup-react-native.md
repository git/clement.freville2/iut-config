React Native à l'IUT
====================

L'installation de `react-native` prend beaucoup de place, ce qui a de grandes chances de dépasser les quotas du home universitaire.

Il est nécessaire d'avoir suffisament de place sur scratch, sinon de nombreuses erreurs -122, de création de fichiers ou d'`unlink` se feront ressentir.

Ce guide détaille les étapes pour correctement utiliser React Native à l'IUT. Nous utilisons NPM ici, mais les instructions indiquées devraient fonctionner sur les autres gestionnaires de paquets modulo quelques modifications.

Rediriger le cache NPM
----------------------

NPM utilise un cache sur le système de gestion de fichiers, sur `~/.cache` par défaut. L'installation de React Native va très probablement faire exploser la taille de ce dossier, d'où l'intérêt de le déplacer sur scratch.

Créer le dossier de cache (`$USER` fait référence à l'utilisateur courant) :

```bash
mkdir /home/scratch/$USER/npm-cache
```

Éditer le fichier `~/.npmrc` pour utiliser ce nouveau dossier, remplacer `$USER` par l'utilisateur courant :
```
cache=/home/scratch/$USER/npm-cache
```

Tunnel
------

Pour tester votre application sans être sur le réseau local de l'IUT (*eduoram* n'est pas le même réseau que les ordinateurs du département), vous pouvez réaliser un tunnel ngrok.

```bash
npm i @expo/ngrok@^4.1.0
npx expo start --tunnel
```

Web
---

Pour tester en local, vous pouvez utiliser le web avec React Native, ce qui nécessite des installations supplémentaires. Vous devrez également modifier les `NODE_OPTIONS` si l'erreur liée à `_Hash` (`error:0308010C:digital envelope routines::unsupported`) se présente.

```bash
npm i react-native-web react-dom @expo/metro-runtime
export NODE_OPTIONS="--openssl-legacy-provider" && npx expo start --web
```

Vous pouvez exporter cette variable d'environnement dans votre `~/.bashrc`.

Fast refresh ([#23104](https://github.com/expo/expo/issues/23104))
------------

L'actualisation rapide consiste à mettre à jour les composants de votre application sans charger complètement la page une deuxième fois.

Pour en bénéficier, créer un point d'entrée qui l'utilise.

```bash
npm install --save-dev @expo/metro-runtime
echo "import '@expo/metro-runtime';
import { registerRootComponent } from 'expo';
import App from './App';

registerRootComponent(App);" > AppEntry.js && \
  sed -i 's|"node_modules/expo/AppEntry.js"|"AppEntry.js"|' package.json
```
